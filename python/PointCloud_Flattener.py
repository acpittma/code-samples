# Basic Script to "Flatten" a point cloud by zeroing out Z values

import geomagic.app.v2
for m in geomagic.app.v2.execStrings: exec m in locals(), globals()

activeModel = geoapp.getActiveModel()

#Convert From Meters to Inches
z_val = 3.5
unit_conversion = 0.0254
z_val = z_val * unit_conversion

#Iterate Through Active Model and set all Z values to specific value
if activeModel:
    pts = geoapp.getPoints( activeModel )
    if pts:
        # Iterate over points 
        for i in pts:
            coord =  pts.getPosition( i )
            pts.setPosition( i, Vector3D( coord.x(), coord.y(), z_val ) )

#Redraw Graphics View
geoapp.redraw(True)
