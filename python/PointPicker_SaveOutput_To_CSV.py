# Imports
import csv
import re
import os
import geoappall
for m in geomagic.app.v2.execStrings: exec m in locals(), globals()
from geomagic.app.v2.gui.quickinput import *

# Get the active model.
activeModel = geoapp.getActiveModel()

#SaveDirectory for CSV file
SaveDir = 'c:\\Users\\apittman.GAD\\Desktop\\CSVTest\\'
FullPath = os.path.join(SaveDir,activeModel.name)

#Conversion Factor (Zero is Meters)
factor = 1000

if activeModel != None:
   print("Model name = " + activeModel.name)
else:
   print("No model selected")

pointName = ["First Point", "Second Point", "Third Point"]

# Run the picker
for i in range(0,len(pointName)):
	res = pickPointOnModel((str(pointName[i])))
	pt = PointFeature()
	pt.name = str(pointName[i])	
	pt.position = Vector3D(res)
	geoapp.addFeature(activeModel, pt)
	geoapp.redraw(True)

# Make sure there is a model selected.
if activeModel != None:
	# Returns a list of all the features attached to the active object.
	features = geoapp.getFeatures(activeModel)
	# Write CSV
	with open(FullPath + '.csv', 'wb') as csvfile:
		mywriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_NONE)
		for feature in features:
			mywriter.writerow( [ feature.name, str(feature.position.x()*factor), str(feature.position.y()*factor), str(feature.position.z()*factor) ] )
	print('CSV File Creation Successful.')
	os.startfile(SaveDir)

else:
	print('CSV File Creation Failed.')