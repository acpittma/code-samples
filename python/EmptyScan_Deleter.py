import geomagic.app.v2
for m in geomagic.app.v2.execStrings: exec m in locals(), globals()

# Get a list of all models in this file.
models = geoapp.getModels()
model_list = []

# Iterate through list of model and delete empty unordered data.
for model in models:
	if geoapp.getPoints(model) == None:
		continue
	points = Points()
	points = geoapp.getPoints(model)
	ordered_points = geoapp.getPoints(model)
	if points.numPoints <= 0:
		model_list.append(model.id)
		
		
# Iterate through list of model and delete empty ordered data.
for model in models:
	if geoapp.getGriddedPoints(model) == None:
		continue
	ordered_points = Points()
	ordered_points = geoapp.getGriddedPoints(model)
	if ordered_points.numPoints <= 0:
		model_list.append(model.id)		

# Delete Models
for modelid in model_list:
	model = geoapp.getModelById(modelid)
	modelname = model.name
	print modelname + ' has been deleted.'
	geoapp.deleteModel(model)

# This is essential, otherwise the deleted points are still visible.
geoapp.redraw(True)

# Note: Does not currently work with objects in groups/folders as they are not currently iterable.