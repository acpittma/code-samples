// public/js/services/DatabaseServices.js
var app = angular.module('DatabaseServices', []);

//Templating Services ======================================================
app.factory('TemplateService', function ($http) {
    var getTemplate = function (content) {
		if (content != null) {
			return $http.get('/js/templates/' + content + '.html');
		}
    };
    return {
        getTemplate: getTemplate
    };
});


//Common Database Services =================================================
app.factory('CommonDB', function($http) {
    return {
		loadReports: function(type) {
			var url = "/api/reports/"+type;
			return $http.get(url);
		},			
		loadMap: function(type) {
			var url = "/api/common/Map/"+type;
			return $http.get(url);
		},		
		storeMap: function(type,input){
			var url = "/api/common/Map/"+type;
			var dataObj = {};
			var data = angular.copy(input);
			
			//Process Data into Mongo Object
			for (i in data){
				delete data[i].error; 
			}
			dataObj[type] = data;
			
			//Make Request
			return $http.post(url,dataObj);
		}		
    };
});


//Parature Database Services ================================================
app.factory('ParatureDB', function($http,$q) {
    return {
		loadSchema: function(type) {
			var url = "/api/parature/Schema/"+type;
			return $http.get(url);
		},
		loadEntities: function(type) {
			var url = "/api/parature/"+type;
			return $http.get(url);
		}

    };
});


//Salesforce Database Services ===============================================
app.factory('SalesforceDB', function($http) {
    return {
		loadSchema: function(type) {
			if (type == 'Customer') {type = 'Contact'}
			var url = "/api/salesforce/Schema/"+type;
			return $http.get(url);
		}		
    };
});


		