// public/js/controllers/FormCtrl.js
var app = angular.module('SyncCtrl', ['DatabaseServices', 'dndLists']);

app.controller('SyncController', function($scope, $modal, ParatureDB, SalesforceDB, CommonDB) {

	// Tagline ================================================================
	$scope.tagline = 'A path between Salesforce and Parature.';
	
	// Data Mapping Model =====================================================
	$scope.mapping = {
		
        lists: {
			parature: {
				allowed: ['parature'],
				fields: []
			},
			salesforce: {
				allowed: ['salesforce'],
				fields: []				
			}
		},
		map: {
			type: '',
			entities: [
				{
					error: {
						msg: '',
						type:  ''
					},
					parature: [],
					salesforce: []
				}	
			]			
		}
    };
		
	// Load Schema ===========================================================
	$scope.loadschema = function(schematype) {
						
		// Load Parature Schema Methods
		ParatureDB.loadSchema(schematype).success(function(data) {

			//Clear Out Existing Scopes
			$scope.mapping.lists.parature.fields  = [];	
			$scope.mapping.map.entities = [];
			
			//Populate Mapping Object "Type"
			$scope.mapping.map.type = schematype;
				
			//Populate Schema Scope
			var pSchema = data[Object.keys(data)[2]];			

			//Populate Fields and Build Mapper Placeholders
			for (obj in pSchema){
				pSchema[obj]['name'] = obj; //Add "name" value pair to call easier later
					
				//Populate and fetch fields that are entities (ie Sla, Csr, Product, etc)
				if (pSchema[obj]['@data-type'] == 'entity') {
					for (field in pSchema[obj]) {
						if (field == 'Customer') {break;}
						if (field == 'Account') {break;}
						if (typeof pSchema[obj][field] == 'object') {
							//Capture iterator in enclosure for handling async promise
							(function(field,obj) {
								ParatureDB.loadEntities(field).success(function(data) {
									if (data.error) { return null; }
									pSchema[obj][field] = data;
								});
							})(field,obj);	
						}	
					}
				}
				
				//Update Scope
				$scope.mapping.lists.parature.fields.push(pSchema[obj]);
				$scope.mapping.map.entities.push(
					{
						error: {
							msg: '',
							type:  ''
						},
						parature: [], 
						salesforce: []
					}
				);
			}		

		});
		
		SalesforceDB.loadSchema(schematype).success(function(data) {
			
			//Clear Out Existing Scopes
			$scope.mapping.lists.salesforce.fields = [];			
			
			//Populate Mapping Object "Type" (if not already populated)
			$scope.mapping.map.type = schematype;
			
			//Populate Schema Scope			
			var sSchema = data[Object.keys(data)[1]];
			
			//Populate Mapping Scope "Fields"
			for (obj in sSchema){
				$scope.mapping.lists.salesforce.fields.push(sSchema[obj]);
			}
			
		});

	}
	
	// Load Current Mapping ===========================================================
	$scope.loadmap = function() {
		
		var type = $scope.mapping.map.type;
		var plist = $scope.mapping.lists.parature.fields;
		var slist = $scope.mapping.lists.salesforce.fields;
						
		// Load Parature Schema Methods
		CommonDB.loadMap(type).success(function(data) {
			for (i in data[type]) {
				
				//Add Error Message Object (Used in Validator)
				data[type][i]['error'] = {msg: '', type:  ''}

				//Get Only Populated Mapped Fields
				if (data[type][i].parature.length != 0){
					var paraMapItem = data[type][i].parature[0];
				}
				if (data[type][i].salesforce.length != 0){
					var salesMapItem = data[type][i].salesforce[0];
				}			
				
				//Iterate Through Sidebar Lists and Remove Matches
				for (p in plist) {
					if (plist[p].name == paraMapItem.name) {	
						plist.splice(p,1);
					}
				}						
				for (s in slist) {
					if (slist[s].name == salesMapItem.name) {	
						slist.splice(s,1);
					}
				}
				
			}
			$scope.mapping.map.entities = data[type];	
		});

	}	
	
	// Validation ==================================================================
	$scope.validator = function(scp){
		
		//Default Status
		var status;
		
		//Validation Logic
		if (!!scp.parature.length && !!scp.salesforce.length) {
			if ( scp.parature[0]['@data-type'] == scp.salesforce[0]['type'] ) { status = 'success-both-match'; }		
			else if ( !!scp.parature[0]['Option'] && !!scp.salesforce[0]['picklistValues'] ) { status = 'success-both-match-options'; }		
			else { status = 'warning-both-mismatch'; }		
		}
		else if (!scp.parature.length && !!scp.salesforce.length) { status = 'error-parature-none';  }
		else if (!scp.salesforce.length && !!scp.parature.length) { status = 'error-salesforce-none'; }
		else if (!scp.parature.length && !scp.salesforce.length) { status = 'error-both-none'; }

		//Error Code Switchblock
		switch(status) {
			case 'error-both-none':
				scp.error.type = 'danger';				
				scp.error.msg = 'No Fields Selected!';
				return null;				
			case 'error-parature-none':
				scp.error.type = 'danger';				
				scp.error.msg = 'No Parature Field Selected!';
				return 'has-error';							
			case 'error-salesforce-none':
				scp.error.type = 'danger';
				scp.error.msg = 'No Salesforce Field Selected!';
				return 'has-error';
			case 'warning-both-mismatch':
				scp.error.type = 'warning';
				scp.error.msg = 'Check that data-types match!';
				return 'has-warning';
			case 'success-both-match-options':
				scp.error.type = 'success';
				scp.error.msg = 'Everything Looks Good! (Check to see that options match)';
				return 'has-success';			
			case 'success-both-match':
				scp.error.type = 'success';
				scp.error.msg = 'Everything Looks Good!';
				return 'has-success';
			default:
				return null;
		}	
		
	}
			
	// Search ======================================================================	
	$scope.search = '';
	
	// Popover =====================================================================	
	//Popover Helper Functions
	var hidePopover = function(currentScope){
		currentScope.$$childHead.isOpen = false;
	}
	
	//Popover Configuration
	$scope.popover = {
		templateUrl: '/js/templates/popover.html',
		hide: hidePopover
	}
		
	// Modal =====================================================================
	//Modal Helper Functions
	var ModalInstanceCtrl = function($scope, $modalInstance, $modal, data) {
		$scope.data = data;
		$scope.miniMap = {};
		
		//Used To Find Array Key Value for MiniMapper
		for (a in data) {
			if (data[a].length) {
				for (b in data[a]){			
					delete data[a][b].error; 						//Delete Error (may want to keep this around later)
					for (c in data[a][b]) {							//Get constant for Entity/Option picklists to use
						if (typeof data[a][b][c] != 'string'){
							$scope.miniMap[a] = c;
						}
					}	
				}
			}	
		}	
		
		$scope.submit = function () {
			$modalInstance.close(data);
		};		  
		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
		};
	}
	
	// Submit (Modal Behavior)
	$scope.modal = function(type,index) {
		
		//Modal Config
		$scope.options = {
			backdrop: true,
			size: 'lg',
			backdropClick: true,
			dialogFade: false,
			keyboard: true,
			templateUrl : '/js/templates/'+type+'.html',
			controller : ModalInstanceCtrl,
			resolve: {} //used to pass items to modal
        };
          
		//Modal Data  
        $scope.options.resolve.data = function() {
			if (index > -1) {
				return angular.copy($scope.mapping.map.entities[index]); // If index is passed in then use that specific entity
			} else {
				return angular.copy($scope.mapping.map); // An object can also be passed if needed
			}
        }
        
		//Start Modal
		var modalInstance = $modal.open($scope.options);

		//Modal Actions
		modalInstance.result.then(function(data){
			if (index > -1) { 
				$scope.mapping.map.entities[index] = data; 
			} else {
				CommonDB.storeMap($scope.mapping.map.type, $scope.mapping.map.entities)
				.success(function(data) {
					alert('Mapping Stored!');
				})
				.error(function(data) {
					alert('Error Communicating with Database: \n' + data);
				});					
			}
		},function(){
			console.log("Modal Closed"); 	//on cancel button press
		});
    };	
		
		
});	