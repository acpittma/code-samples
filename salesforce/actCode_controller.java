/*
Info: Activation Code Lookup Controller
Created By: Austin Pittman, 3D Systems, Inc.
Create Date: Mar 17, 2014
Updated: Mar 25, 2014
*/

global without sharing class ActCodeLookupController{
    public ActCodeLookupController(ApexPages.StandardController c) {}

    @RemoteAction
    global static String doSubmitForm(String actCode) {
        if(actcode != null) {
            Asset ass = getAsset(actCode);
            if(ass!= null) {
                Datetime maint = ass.Maintenance_Expiration__c;         
                string maintenance = maint.format('MM/dd/yyyy','America/New_York');
                string name = ass.Product2.Product_Category__c;
                List<String> info = new List<String> { name, maintenance };
                return JSON.serialize(info);
            } else {
                return JSON.serialize('Invalid Activaton Code Entered');
            }
        } else {
            return 'Activation Code was null';
        }
    }
     
    private static Asset getAsset(String actCode) {
        list<Asset> assets;
        system.debug(actcode);
    if (actcode.right(4).indexOf('XXXX') != -1) {
      assets =  [SELECT Product2.Product_Category__c,Maintenance_Expiration__c
              FROM Asset
              WHERE Masked_Key__c like : actCode
              AND Product2.Name like '%Dongle%'
              LIMIT 1];
            system.debug(assets);
    } else {
      assets =  [SELECT Product2.Product_Category__c,Maintenance_Expiration__c
              FROM Asset
                           WHERE License_Key__c like : actCode
                          LIMIT 1];
            system.debug(assets);            
    }
        if(assets.isEmpty()) {
            return null;
        } else {
            return assets[0];
        }
    }
}