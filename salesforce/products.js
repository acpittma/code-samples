jsonpCallback([    
    {
    "product": "Geomagic Capture for SC",
    "installer": [
        {
            "relDate": "2015-02-12",    
            "x64": "https://3dsystems.teamplatform.com/pages/66608?t=roborm9w2p1t",
            "version": "2014.3.0",            
            "relNotes": ""
        },{
            "relDate": "2014-10-09",    
            "x64": "https://3dsystems.teamplatform.com/l/8ca1fae8",
            "x86": "https://3dsystems.teamplatform.com/l/f4c4e560",
            "version": "2014.2.0",            
            "relNotes": ""
        },{             
            "relDate": "2013-12-19",    
            "x64": "https://3dsystems.teamplatform.com/l/e5d80983",
            "x86": "https://3dsystems.teamplatform.com/l/1a6541da",
            "version": "2014.1.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Capture for SW",
    "installer": [
        {
            "relDate": "2015-01-27",    
            "x64": "https://3dsystems.teamplatform.com/pages/66608?t=roborm9w2p1t",
            "version": "2014.3.0",            
            "relNotes": ""
        },{     
            "relDate": "2014-08-21",    
            "x64": "https://3dsystems.teamplatform.com/l/cfb83a20",
            "x86": "https://3dsystems.teamplatform.com/l/d12724d3",
            "version": "2014.2.0",            
            "relNotes": ""
        },{     
            "relDate": "2013-12-19",    
            "x64": "https://3dsystems.teamplatform.com/l/1cd7a3f4",
            "x86": "https://3dsystems.teamplatform.com/l/7c21197a",
            "version": "2014.1.0",                
            "relNotes": ""
        }]
    },{     
    "product": "Geomagic Design",
    "installer": [
        {
            "relDate": "2015-09-29",    
            "x64": "https://3dsystems.teamplatform.com/pages/67155?t=60dvsqblb067",
            "addOns": [],
            "version": "2015.1.0",
            "relNotes": ""
        },{
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/l/c175164b",
            "x86": "https://3dsystems.teamplatform.com/l/76a911c1",
            "addOns": [],
            "version": "2015.0.1",
            "relNotes": ""
        },{ 
            "relDate": "2015-02-10",    
            "x64": "https://teamplatform.com/l/c1d6328c",
            "x86": "https://teamplatform.com/l/a11b7765",
            "addOns": [],
            "version": "17.0.4",          
            "relNotes": ""            
        },{     
            "relDate": "2014-08-17",    
            "x64": "https://3dsystems.teamplatform.com/l/ba187e8a",
            "x86": "https://3dsystems.teamplatform.com/l/6d9baed1",
            "addOns": [],         
            "version": "16.0.2",          
            "relNotes": ""
        },{
            "relDate": "2014-05-12",    
            "x64": "https://3dsystems.teamplatform.com/l/ba187e8a",
            "x86": "https://3dsystems.teamplatform.com/l/6d9baed1",
            "addOns": [],         
            "version": "16.0.1",          
            "relNotes": ""
        },{
            "relDate": "2014-03-10",    
            "x64": "https://3dsystems.teamplatform.com/l/ba187e8a",
            "x86": "https://3dsystems.teamplatform.com/l/6d9baed1",
            "addOns": [],         
            "version": "16.0",            
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Design Direct",
    "installer": [
        {
            "relDate": "2015-01-27",    
            "x64": "https://3dsystems.teamplatform.com/pages/66602?t=sy4j3zih91p3",
            "version": "2014.3.0",                
            "relNotes": ""
        },{     
            "relDate": "2014-10-09",    
            "x64": "https://3dsystems.teamplatform.com/l/7fe9d445",
            "x86": "https://3dsystems.teamplatform.com/l/21bf2e36",
            "version": "2014.2.0",                
            "relNotes": ""
        },{
            "relDate": "2013-12-19",    
            "x64": "https://3dsystems.teamplatform.com/l/1621c19c",
            "x86": "https://3dsystems.teamplatform.com/l/0be78a91",
            "version": "2014.1.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Design X",
    "installer": [
        {
            "relDate": "2015-10-19",    
            "x64": "https://3dsystems.teamplatform.com/pages/66601?t=4rrsrjnwroxj",
            "version": "2016.0.0",             
            "relNotes": ""
        },{
            "relDate": "2015-03-24",    
            "x64": "https://teamplatform.com/l/fca32162",
            "version": "5.2",             
            "relNotes": "https://teamplatform.com/l/3c19baf0"
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/9e7322de",
            "x86": "https://teamplatform.com/l/a401dee7",
            "version": "5.1",             
            "relNotes": ""
        },{     
            "relDate": "2014-08-21",    
            "x64": "https://3dsystems.teamplatform.com/l/f900f21f",
            "x86": "https://3dsystems.teamplatform.com/l/c0d17f60",
            "version": "5.0",             
            "relNotes": "https://3dsystems.teamplatform.com/l/3159a515"
        },{
            "relDate": "2013-12-16",    
            "x64": "https://3dsystems.teamplatform.com/l/1608e29f",
            "x86": "https://3dsystems.teamplatform.com/l/8857689d",
            "version": "4.1.0",               
            "relNotes": ""
        }]
    },{ 
    "product": "Geomagic Control",
    "installer": [
        {
            "relDate": "2015-09-29",    
            "x64": "https://3dsystems.teamplatform.com/pages/66603?t=9dzwbud4ms3p",
            "version": "2015.0.0",                
            "relNotes": ""
        },{
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/l/00f4cc90",
            "version": "2014.4.0",                
            "relNotes": ""
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/865c1149",
            "x86": "https://teamplatform.com/l/aeed9bcf",
            "version": "2014.3.0",                
            "relNotes": ""
        },{     
            "relDate": "2014-08-28",    
            "x64": "https://3dsystems.teamplatform.com/l/ec589737",
            "x86": "https://3dsystems.teamplatform.com/l/f8b5f60f",
            "version": "2014.2.0",                
            "relNotes": ""
        },{
            "relDate": "2013-12-19",    
            "x64": "https://3dsystems.teamplatform.com/l/3f3cf845",
            "x86": "https://3dsystems.teamplatform.com/l/5f5a45b5",
            "version": "2014.1.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Studio",
    "installer": [
        {
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/pages/66601?t=4rrsrjnwroxj",
            "version": "5.2",
            "relNotes": "",
			"migrated": true
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/865c1149",
            "x86": "https://teamplatform.com/l/aeed9bcf",
            "version": "2014.3.0",                
            "relNotes": ""
        },{ 
            "relDate": "2014-08-28",    
            "x64": "https://3dsystems.teamplatform.com/l/ec589737",
            "x86": "https://3dsystems.teamplatform.com/l/f8b5f60f",
            "version": "2014.2.0",                
            "relNotes": ""
        },{
            "relDate": "2013-12-19",    
            "x64": "https://3dsystems.teamplatform.com/l/3f3cf845",
            "x86": "https://3dsystems.teamplatform.com/l/5f5a45b5",
            "version": "2014.1.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Wrap",
    "installer": [
        {
            "relDate": "2015-09-29",    
            "x64": "https://3dsystems.teamplatform.com/pages/66603?t=9dzwbud4ms3p",
            "version": "2015.0.0",                
            "relNotes": ""
        },{
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/l/00f4cc90",
            "version": "2014.4.0",                
            "relNotes": ""
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/865c1149",
            "x86": "https://teamplatform.com/l/aeed9bcf",
            "version": "2014.3.0",                
            "relNotes": ""
        },{ 
            "relDate": "2014-08-28",    
            "x64": "https://3dsystems.teamplatform.com/l/ec589737",
            "x86": "https://3dsystems.teamplatform.com/l/f8b5f60f",
            "version": "2014.2.0",                
            "relNotes": ""
        },{
            "relDate": "2013-12-19",    
            "x64": "https://3dsystems.teamplatform.com/l/3f3cf845",
            "x86": "https://3dsystems.teamplatform.com/l/5f5a45b5",
            "version": "2014.1.0",                
            "relNotes": ""
        }]
    },{ 
    "product": "Geomagic Freeform",
    "installer": [
        {
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/pages/66612?t=1wc4iaho50sc",
            "version": "2015.0.0",                
            "relNotes": ""        
        },{     
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/d7c3be6a",
            "x86": "https://teamplatform.com/l/95b7c614",
            "version": "2014.3.0",                
            "relNotes": ""        
        },{
            "relDate": "2014-04-02",    
            "x64": "https://3dsystems.teamplatform.com/l/6916a0fa",
            "x86": "https://3dsystems.teamplatform.com/l/032b56b9",
            "version": "2014.2.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Freeform Plus",
    "installer": [
        {
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/pages/66612?t=1wc4iaho50sc",
            "version": "2015.0.0",                
            "relNotes": ""        
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/c5fb43c9",
            "x86": "https://teamplatform.com/l/78d03f86",
            "version": "2014.3.0",                
            "relNotes": ""        
        },{     
            "relDate": "2014-04-02",    
            "x64": "https://3dsystems.teamplatform.com/l/34959efc",
            "x86": "https://3dsystems.teamplatform.com/l/8d5461df",
            "version": "2014.2.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Sculpt",
    "installer": [
        {
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/pages/82565?t=3iq6w5o0cfaa",
            "version": "2014.3.0",                
            "relNotes": ""        
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://teamplatform.com/l/378368e4",
            "x86": "https://teamplatform.com/l/13ad5181",
            "version": "2014.3.0",                
            "relNotes": ""        
        },{     
            "relDate": "2014-10-21",    
            "x64": "https://3dsystems.teamplatform.com/l/7e6d00f7",
            "x86": "https://3dsystems.teamplatform.com/l/c6685885",
            "version": "2014.2.0",                
            "relNotes": ""
        }]
    },{
    "product": "Geomagic Verify",
    "installer": [
        {
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/pages/66603?t=9dzwbud4ms3p",
            "version": "2014.4.0",                
            "relNotes": "",
			"migrated": true			
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://3dsystems.teamplatform.com/pages/66604?t=4k8n4ufegzmz",
            "version": "5.1",             
            "relNotes": ""
        },{     
            "relDate": "2014-08-21",    
            "x64": "https://3dsystems.teamplatform.com/l/e70c80e8",
            "x86": "https://3dsystems.teamplatform.com/l/1543220a",
            "version": "5.0",             
            "relNotes": "https://3dsystems.teamplatform.com/l/34fadeee"
        },{
            "relDate": "2013-12-16",    
            "x64": "https://3dsystems.teamplatform.com/l/4ed3fb00",
            "x86": "https://3dsystems.teamplatform.com/l/6129cb47",
            "version": "4.1.0",               
            "relNotes": ""
        }]
    },{
    "product": "Geomagic XOS",
    "installer": [
        {
            "relDate": "2015-03-24",    
            "x64": "https://3dsystems.teamplatform.com/pages/66603?t=9dzwbud4ms3p",
            "version": "2014.4.0",                
            "relNotes": "",
			"migrated": true
        },{         
            "relDate": "2015-01-27",    
            "x64": "https://3dsystems.teamplatform.com/pages/66613?t=amuauuh3nrwa",
            "version": "5.1",             
            "relNotes": ""
        },{     
            "relDate": "2014-08-21",    
            "x64": "",
            "x86": "",
            "version": "5.0",             
            "relNotes": "https://3dsystems.teamplatform.com/l/3159a515"       
        },{         
            "relDate": "2013-12-16",    
            "x64": "https://3dsystems.teamplatform.com/l/ac72763c",
            "x86": "https://3dsystems.teamplatform.com/l/b722c7f4",
            "version": "4.1.0",               
            "relNotes": ""
        }]
    }
]);

