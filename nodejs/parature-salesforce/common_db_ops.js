// app/models/common_db_ops.js

// MODULES ===========================================================
var db		= require('../../db.js')

// METHODS ===========================================================

//Fetch Resource - Takes Database, Type, and Resource (Query) Parameters
var fetch = function(request, result, callback) {
	
	//Get Parameters From Request
    var database = request.params.database;
    var type = request.params.type;
    var resource = request.params.resource;

	//Set Collection
	var collection = db.root.db(database).collection(type);

	//Build Search Query Object
	switch(true) {
		case (result != null): //API Call - Query If Resource Exists 
			var query = {};
			query[resource] = {$exists:true};
			break;
		case (callback != null):  //Internal Call - Resource should be a query object
			query = resource;
			break;
	}	
		
	//Database Query
	collection.findOne(query, function(err, item) {
		switch(true) {
			case (result != null):	//API Call	
				if (err) { result.json(err); }
				else if (item) { result.json(item); }
				else { result.json({'error': 'Invalid Request. Please read API documentation for API Request structure.'}); }
				break;
			case (callback != null): //Internal Call		
				if (err) { callback(err); }
				else if (item) { callback(null,item); }
				else { callback( {error: JSON.stringify(type) + ' not found!' } ); }
				break;				
		}		
	});	
}


//Fetch Resource By Type (Account, Customer, Etc)
var fetchByType = function(request, result, callback) {
		
	//Get Parameters From Request
	var database = request.params.database;
    var type = request.params.type;
		
	//Set Collection
	var collection = db.root.db(database).collection(type);
	
	//Database Query - Sorts by descending natural order (last added first in resulting array)
	collection.find().sort({$natural:-1}).toArray(function (err, doc){
		switch(true) {
			case (result != null):	//API Call	
				if (err) { result.json(err); }
				else if (doc.length > 0) { result.json(doc); }
				else { result.json({'error': 'No item(s) found. Please read API documentation for API Request structure.'}); }
				break;
			case (callback != null): //Internal Call		
				if (err) { callback(err); }
				else if (doc.length > 0) { callback(null,doc); }
				else { callback( {error: JSON.stringify(type) + ' not found!' } ); }
				break;				
		}		
	});				
}


//Store Resource
var store = function(request, result, callback) {
	
	//Get Parameters and Body From Request
    var database = request.params.database;
    var type = request.params.type;
    var resource = request.params.resource;
	var data = request.body;
	
	//Set Collection
	var collection = db.root.db(database).collection(type);
		
	//Build Search Query Object 
	switch(true) {
		case (result != null): //API Call - Query If Resource Exists 
			var query = {};
			query[resource] = {$exists:true};
			break;
		case (callback != null):  //Internal Call - Resource should be a query object
			query = resource;
			break;
	}
	
	//Upsert Document to Collection
	collection.findAndModify(
		query, 										//Query
		[], 										//Sort
		{ $set: data },								//Update Statement
		{ upsert: true, new: true},					//Options
		function(err,res) {							//Callback
			switch(true) {
				case (result != null):	//API Call
					if (err) { result.json(err) } 				
					else if (res.lastErrorObject.updatedExisting == true) { 
						var msg = type + resource + 'has been updated.'; 
						result.json({'success': msg}); 
					} else { 
						var msg = type + resource + 'has been added.'; 
						result.json({'success': msg});					
					}
					break;
				case (callback != null): //Internal Call
					if (err) { callback(err); return null; }

					callback(null,res)
					break;					
			}
		}
	);
}


// EXPORTS =====================================================================
module.exports = {
	fetch: fetch,
	fetchByType: fetchByType,
	store: store
}