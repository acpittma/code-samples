// app/services/poll_parature.js

// MODULES ===========================================================

//Load Modules
var configAuth 	= require('../../config/auth.js');
var dbOps  		= require('../services/common_db_ops.js');
var dbHelpers	= require('../services/common_db_helpers.js');
var paraOps		= require('../services/parature_api_ops.js');


// CONFIG ==============================================================

// Load Auth Variables
var clientID = configAuth.ParatureAuth.clientID;
var deptID = configAuth.ParatureAuth.deptID;
var token = configAuth.ParatureAuth.token;
var paratureUrl = configAuth.ParatureAuth.url;

//Global Constants
var pageSize  		= 500;
var delay	  		= 500;


// Parature Requests ============================================================
	
// Initialize Requests - Used As An Export, Checks For Abort Event Condition, and Starts Polling
var init = function init(type,callback){
	console.log('Polling Parature %s(s)...',type)
	
	abort = false;
	report = null;
	total			= 0;
	totalNew		= 0;
	totalUpdated	= 0;
	totalSkipped	= 0;
	startTime 		= new Date().toISOString();	//process.hrtime()

	process.on('abort', function(){ abort = true; });	
	
	pollParature(type,callback);	
}

//Poll Parature Job - Accepts a Type and a Callback Function (To Tell When Job Is All Done)
function pollParature(type,master){
	
	//Start By Grabbing Total
	fetchTotal();
	
	//Fetch Total Number of Accounts, Customers, Etc
	function fetchTotal(){
		var uri = type;
		var params = '&_total_=true';
		
		paraOps.get(uri,params,function(err,res){
			if (err) {console.log(err); return null;}
			
			total = res.Entities['@total'];
			var page = Math.ceil(total / pageSize);
			fetchEntities(page);
		});
	}
	
	
	//Fetch All Entities On Single Page
	function fetchEntities(page){
		var uri = type;
		var params = '&_pageSize_='+pageSize+'&_startPage_='+page;

		paraOps.get(uri,params,function(err,res){
			if (err) {console.log(err); return null;}
			
			var res = res.Entities[type];
			fetchEntity(res,page);
		});	
	}
	
	
	//Fetch Information For Single Entity - Get A Single Entity, Make Get Request, Recursively Call Fetch Functions As Needed
	function fetchEntity(res,page,index){
		if (page <= 0) { return null; }
		if (index == null) { index = res.length - 1; }
	
		var uri = type+'/'+res[index]['@id'];
		var params = '';
		
		paraOps.get(uri,params,function(err,res){
			if (err) {console.log(err); return null;}
			
			toDatabase(res[type]);
		});
				
		if (abort == true) { 
			var jobInfo = { job: report };
			master(jobInfo);
			return null;
		}
		else if (index > 0) { setTimeout(fetchEntity, delay, res, page, index-1); }
		else { fetchEntities(page-1);  }	
	}

	
	//Store Information to MongoDB - Build MongoDB Request and Make Request
	function toDatabase(res){
		
		var req = { params: {} };
		req.params.database = 'parature';
		req.params.type = type;
		req.params.resource = { '@id' : res['@id'] };
		req.body = dbHelpers.parseParatureResult(res); 
				
		dbOps.store(req, null, function(err,res){
			if (err) {console.log(err); return null;}	
			if (res.lastErrorObject.upserted) { totalNew++; }
			else if (res.lastErrorObject.updatedExisting == true) { totalUpdated++; }
			
			toReport();
		});	
	}
	
	
	//Create Basic Report - The Report
	function toReport(){
			
		var req = { params: {} };
		req.params.database = 'reports';
		req.params.type = type;
		if (report) { req.params.resource = { _id: report } }		
		else { req.params.resource = {Date_Started: startTime} }		
		req.body = { 
					Date_Started: startTime,
					Parature_Total: total,
					Parature_Added: totalNew,
					Parature_Updated: totalUpdated,
					Parature_Skipped: totalSkipped,
					Parature_Estimate: total * 550 - ((totalNew + totalUpdated + totalSkipped) * 550)	//Assumption of 550ms to complete one record (500ms throttle limit considered)				
					}
				
					
		dbOps.store(req, null, function(err,res){
			if (err) {console.log(err); return null;}

			if (!report) { report = res.value._id; }	
			if (totalNew + totalUpdated == total) { master(null,res.value._id); }	
		});
	}
	
}


// EXPORTS =====================================================================
module.exports.init = init;