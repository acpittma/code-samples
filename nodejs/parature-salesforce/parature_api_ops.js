// app/models/common_db_ops.js

// MODULES ===========================================================

var XmlParse 	= require('xml2json');
var request 	= require('request');
var db			= require('../../db.js')
var configAuth 	= require('../../config/auth.js');

// CONFIG ==============================================================

var clientID = configAuth.ParatureAuth.clientID;
var deptID = configAuth.ParatureAuth.deptID;
var token = configAuth.ParatureAuth.token;
var paratureUrl = configAuth.ParatureAuth.url;

// METHODS ===========================================================

//Get Operation
var get = function(uri,url_params,caller) {
	
	//Parature Callback
	function callback(error, response, body) {
		if (error) { caller(error); return null; }
		
		caller(null,response.body);	
	}

	//Request Options
	var options = {
		url: paratureUrl
			+ clientID +'/'
			+ deptID +'/'
			+ uri +'/'
			+'?_token_='+token
			+'&_output_=json'
			+ url_params,
		json: true,
		encoding: 'utf8',
		method: 'GET'
	};

	//Make Request
	request(options,callback); 
}


//Post Operation
var post = function (type,data,caller) {
	
	//Parature Callback
	function callback(error, response, body) {
		if (error) { caller(error); return null; }

		//Response Handling and Routing
		var res = JSON.parse(XmlParse.toJson(body)); 		//Convert XML response to JSON
		if (res[type]) { caller(null,res[type].id); }	
		else { caller(res) }
	}

	//Request Options
	var options = {
		url: paratureUrl
			+ clientID +'/'
			+ deptID +'/'
			+ type +'/'
			+'?_token_='+token,
		header: 'Content-Type: application/xml; charset=utf-8',
		method: 'POST',
		body: data
	};		

	//Make Request
	request(options,callback); 
}


//Put Operation
var put = function (type,data,id,caller) {

	//Parature Callback
	function callback(error, response, body) {
		if (error) { caller(error); return null; }

		//Response Handling and Routing
		var res = JSON.parse(XmlParse.toJson(body)); 		//Convert XML response to JSON
		if (res[type]) { caller(null,res[type].id); }	
		else { caller(res) }
	}

	//Request Options
	var options = {
		url: paratureUrl
			+ clientID +'/'
			+ deptID +'/'
			+ type +'/'
			+ id
			+'?_token_='+token,
		header: 'Content-Type: application/xml; charset=utf-8',
		method: 'PUT',
		body: data
	};		

	//Make Request
	request(options,callback);	
}


//Delete Operation - Be sure to call twice. Once to put in trash and once to purge trash.
var remove = function(uri,url_params,caller) {
	
	//Parature Callback
	function callback(error, response, body) {
		if (error) { caller(error); return null;}
		
		caller(null,response.body);	
	}

	//Request Options
	var options = {
		url: paratureUrl
			+ clientID +'/'
			+ deptID +'/'
			+ uri +'/'
			+'?_token_='+token
			+ url_params,
		encoding: 'utf8',
		method: 'DELETE'
	};
	
	//Make Request
	request(options,callback); 
}

// EXPORTS =====================================================================
module.exports = {
	get: get,
	post: post,
	put: put,
	remove: remove
}