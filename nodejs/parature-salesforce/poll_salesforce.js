// app/services/salesforce_update_db.js

// MODULES ==============================================================

//Load Modules
var jsforce 		= require('jsforce');
var configAuth  	= require('../../config/auth.js');
var dbOps  			= require('../services/common_db_ops.js');
var dbHelpers		= require('../services/common_db_helpers.js');
var paraCtrl		= require('./post_parature.js'); 

// CONFIG ==============================================================

//Load Auth Variables
var clientID = configAuth.SalesforceAuth.clientID;
var clientSecret = configAuth.SalesforceAuth.clientSecret;
var salesforceUrl = configAuth.SalesforceAuth.redirectUri; //Might only be needed for Oauth

//JSForce Config
var salesforce = new jsforce.Connection({
	loginUrl : 'https://test.salesforce.com'
});


// Salesforce Operations ====================================================

// Initialize Requests (Init function is later used as export)
var init = function init(type,report,callback){
	console.log('Polling Salesforce %s(s)...',type)	
	pollSalesforce(type,report,callback);
}


//Poll Salesforce Job - Accepts a Type and a Callback Function
function pollSalesforce(type,report,master){
	/*
	process.on('abort', function(){
		console.log('Aborting Job at Poll_Salesforce.js...')
		return null;
	});	
	*/
	//Start By Logging Into Salesforce
	salesforceLogin();
	
	
	//Initial Request - Login To Salesforce
	function salesforceLogin(){	
		salesforce.login(clientID, clientSecret, function(err, res) {
			if (err) { return console.log(err); }
			fetchMap();
		});
	}


	//Fetch Parature-Salesforce Map - Fetch Mapping from LocalDB and Pass to SOQL Request
	function fetchMap(){
			
		var req = { params: {} };
		req.params.database = 'common';
		req.params.type = 'Map';
		req.params.resource = {};
		req.params.resource[type] = {$exists:'true'};		
		
		dbOps.fetch(req, null, function(err,res){
			if (err) { console.log(err); return null; }
			
			var map = dbHelpers.scrapeMap(res);
			requestSOQL(map);
		});	
	}

	
	//Make SOQL Query  - Build SOQL Query, Make Salesforce SOQL Request, Pass to MongoDB Controller
	function requestSOQL(map) {
		if (type == 'Customer') {sType = 'Contact'; sCond = ' WHERE AccountId!=null AND Email!=null'} 
		else if (type != 'Account') {sType = type; sCond = ' WHERE AccountId!=null'}
		else {sType = type; sCond = ''/*" WHERE Name='3D Agency'"*/};

		var query = 'SELECT ' + map.fields.toString() + ' FROM ' + sType + sCond; //WHERE LastModifiedDate > LastPoll Cycle (Build SOQL Query)
			
		salesforce.query(query, function(err, res) {
			if (err) { return console.error(err); }
			mongoController(map.map,map.id,map.pid,res);
		});

	}

	
	//Controller To Handle Mongo Requests - Set Initial Totals, Loop Through All Records, Start Building Mongo Request Body
	function mongoController(map,id,pid,res){
		
		//Global Vars To Track Progress & Total
		progress_new = 0;
		progress_updated = 0;
		progress_skipped = 0;
		progress_errors = {};
		progress_errors.total = 0;
		progress_errors.errors = [];
		total = res.records.length;

		if (type == 'Account') {
			
			//Start Keeping Track Of Names
			var claimed = {};
			for (i in res.records){
				var name = res.records[i].Name;
				if ( !(name in claimed) ) { claimed[name] = []; claimed[name].push(i); }
				else { claimed[name].push(i); }
			}	
			
			//Delete Any Non-Duplicate Names From Duplicates
			for (i in claimed) {
				if( claimed[i].length < 2) { delete claimed[i]; }
			}
			
			//Loop Through Duplicates Object and Add Leading Zeros For Any Duplicates	
			for (a in claimed) {		
				var dupeNum = 1;			
				for (b in claimed[a]){
					if (b > 0) {
						var i = claimed[a][b];
						dupeNum++
						if (dupeNum < 10) { leadingNum = ' [00' + dupeNum + ']'; }
						else if (dupeNum < 100) { leadingNum = ' [0' + dupeNum + ']'; }
						
						res.records[i].Name = res.records[i].Name + leadingNum;
					}			
				}
			}
		}

		//Start Iterator (via Index)
		looper(total-1);
		
		//Loop Through Results and Make Requests 
		function looper(index) {
		
			var doc = res.records[index];
			var req = { params: {} };
			req.params.database = 'parature';
			req.params.type = type;
			req.params.resource = dbHelpers.queryExisting(doc,id);
			req.body = dbHelpers.parseSalesforceResult(map,doc); 
			
			//If Parent Exists Then Get It First
			if (req.body[pid]) {
				requestParent(req,req.body[pid]);
			} else {
				toDatabase(req);
			}
			
			//Call Function Recursively for Parature API Throttling Purposes
			if (index > 0) { setTimeout(looper, 500, index-1); }
		}	
	}

	
	//Request Parent Account - Check For Existence of Parent Account, If Parent Exists Add Account, Otherwise Create New
	function requestParent(req,parentId) {
		
		var parentReq = { params: {} };
		parentReq.params.database = 'parature';
		parentReq.params.type = 'Account';
		parentReq.params.resource = { 120207 : parentId };	

		dbOps.fetch(parentReq, null, function(err,res){	
			if (res && res['@id']) {	
				var parentObj = { '@id' : res['@id'] }
				if ( type == 'Customer' ) { req.body.Account = parentObj; }
				if ( type == 'Asset' ) { req.body.Account_Owner = parentObj; }
			}	
			toDatabase(req);
		});
	}
	
	
	//Make MongoDB Request - Make Mongo Request (Get Parent Then Make Request If Need Be)
	function toDatabase(req) {
		dbOps.store(req, null, function(err,res){
			if (err) { console.log(err); return null;}
			
			if ( type == 'Customer' ) { tracker = res.value.Email; }
			else if ( type == 'Account' ) { tracker = res.value.Account_Name; }
			else { tracker = res.value._id; }
			
			toParature(res.value._id, tracker);
		});	
	}


	//Send MongoDB Result to Parature Controller - Passes current Type and mongoId to controller where it performs CRUD operations.
	function toParature(mongoId,tracker){	
		paraCtrl.init(type,mongoId,function(err,res,operation) {	
			if (err) { 
				var errorObj = 	{
									mongoId: mongoId,						
									tracker: tracker,
									error: err.Error.message
								}
				progress_errors.errors.push(errorObj); progress_errors.total++;;		
			}
			else if (operation == 'PUT') { progress_updated++; }
			else if (operation == 'POST') { progress_new++; }
			
			toReport();
		});		
	}

	
	//Create Basic Report
	function toReport(){
		
		var req = { params: {} };
		req.params.database = 'reports';
		req.params.type = type;
		req.params.resource = { _id: report }
		req.body = { 
					Salesforce_Total: total,
					Salesforce_Added: progress_new,
					Salesforce_Updated: progress_updated,
					Salesforce_Skipped: progress_skipped,
					Salesforce_Errors: progress_errors,			
					Salesforce_Estimate: total * 450 - ((progress_new + progress_updated + progress_skipped + progress_errors.total) * 450)		//Assumption of 450ms to complete one record
					}
					
		dbOps.store(req, null, function(err,res){
			if (err) {console.log(err); return null;}
			
			if (progress_new + progress_updated + progress_skipped + progress_errors.total == total) { master(null,res.value._id); }	
		});
	}

}


// EXPORTS =====================================================================
module.exports.init = init;










